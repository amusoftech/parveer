import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

export default function List({users,update}) {
  const [myUsers, setmyUsers] = useState();
  const [singleUser, setSingleUser] = useState ()

  const [currentPage, setCurrentPage] = useState();
  const geUsers = (url) => {
    return fetch(url);
  };

  useEffect(() => {
    const data = geUsers("https://reqres.in/api/users");

    data
      .then((response) => response.json())
      .then((users) => {
         
        setmyUsers(users.data);
        setCurrentPage(users.page);
      })
      .catch((errr) => {
        console.log("Error", errr);
      });
  }, []);

  const getSingleUser = (item) => {
    const singleuser = geUsers(`https://reqres.in/api/users/${item.id}`)

    singleuser
      .then((singleUser_) => singleUser_.json())
      .then((singleUser_) => {
        setSingleUser(singleUser_.data) 
        console.log("singleUser_",singleUser_)
      });
  };

  return (
    <div className={"row d-flex  jsutify-content-center"}>

      {/*SINGLE USER INFO START */}
      { singleUser && 
              <div class="card m-2" style={{ width: "18rem", border:'3px solid yellow' }}>
                <img style={{maxHeight:'150px !important'}} class="card-img-top" src={singleUser.avatar} alt="Card image cap" />
                <div class="card-body">
                  <h5 class="card-title">{`${singleUser.first_name} ${singleUser.last_name}.....`}</h5>
                </div>
              </div>}
{/*SINGLE USER INFO END */}

      {users ? (
        users.map((item, index) => {
          const { userId,  img, name } = item;
          return (
            <Link
            key={index}
            to={{pathname:`/update/${ userId}`}}
              /* onClick={(e) => { 
               update(userId)
              }} */
              //href="/update"
            >
              <div class="card m-2" style={{ width: "18rem" }}>
                <img   style={{height:100, resizeMode:'contain' }}  class="card-img-top" src={img} alt="Card image cap" />
                <div class="card-body">
                  <h5 class="card-title">{`${name}..... ${userId}`}</h5>
                </div>
              </div>
            </Link>
          );
        })
      ) : (
        <div className={"col"}>
          <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>
      )}


    </div>
  );
}
