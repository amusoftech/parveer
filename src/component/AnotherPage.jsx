import React, { memo } from "react";
import { connect } from "react-redux";

function AnotherPage({ users:{allUsers} }) {
   
  return (
    <div>
      <h1>New Page</h1>
      <ul>
    {
        allUsers.map((item,index)=>{
        return   <img   style={{height:100, resizeMode:'contain' }}  class="card-img-top" src={item.img} alt="Card image cap" />
        })
    }
      </ul>
    </div>
  );
}
const mapStateToProps = (state) => ({
  ...state,
});
export default connect(mapStateToProps, null)(memo(AnotherPage));
