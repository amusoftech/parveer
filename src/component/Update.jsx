import React,{useState,useEffect} from 'react'
import firebaseApp from '../utils/firebaseApp';
import readData from '../utils/readData';

export default function Update({match,  history}) {
const [currentUser, setcurrentUser] = useState({})
//const [name$, setName$] = useState()
    const userId =   match && match.params &&    match.params.user
const getUser = ()=>{
    const result = readData("test", userId); 
    result.get().then(doc=>{
        console.log("snapshot",doc.data()); 
        setcurrentUser({...doc.data()})
    })
}

useEffect(() => {
    userId &&  getUser() 
}, [])

const  onSubmit = async (evt) =>{
    evt.preventDefault() 
    await firebaseApp.firestore().collection("test").doc(userId).set({
       name: evt.target.name.value,
     country: evt.target.city.value
    },
    {
        merge:true
    })  
    history.push('/')
}
console.log("props",userId);

    return (
        <div>
            {
            userId ?
            <>
            <h1>Updating data</h1>
            <form onSubmit={onSubmit}>
            <div class="form-group">
          <label htmlFor="exampleInputEname">Email address</label>
          <input
           defaultValue={currentUser.name}
            type="name"
            class="form-control" 
            id="exampleInputEname"
            aria-describedby="emailHelp"
            placeholder="Enter email"
            name={"name"}
          />
          <small id="emailHelp" class="form-text text-muted">
            We'll never share your email with anyone else.
          </small>
        </div>
        <div class="form-group">
          <label htmlFor="city">Password</label>
          <input
          
          defaultValue={currentUser.country}
            type="text"
            class="form-control"
            id="city"
            placeholder="Password"
            name={"city"}
          />
        </div>

        <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input" id="customSwitch1"/>
          <label class="custom-control-label" for="customSwitch1">Toggle this switch element</label>
        </div>


        <button  className={"btn btn-info"} type={"submit"}>Save</button>
            </form>
        </>    
        : 
        <h3>Invalid user id</h3>

        }
        </div>
    )
}
