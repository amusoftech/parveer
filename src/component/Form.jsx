import React from "react";

export default function Form({onSubmit}) {
    /* const onSubmit =(evt)=>{
            evt.preventDefault()
                console.log("evt",evt.target.email.value,evt.target.city.value);

    } */
  return (
    <div>
      <form onSubmit={onSubmit} className={"form-group"}>
        <div class="form-group">
          <label htmlFor="exampleInputEname">Email address</label>
          <input
            type="name"
            class="form-control" 
            id="exampleInputEname"
            aria-describedby="emailHelp"
            placeholder="Enter email"
            name={"name"}
          />
          <small id="emailHelp" class="form-text text-muted">
            We'll never share your email with anyone else.
          </small>
        </div>
        <div class="form-group">
          <label htmlFor="city">Password</label>
          <input
            type="text"
            class="form-control"
            id="city"
            placeholder="Password"
            name={"city"}
          />
        </div>

        <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input" id="customSwitch1"/>
          <label class="custom-control-label" for="customSwitch1">Toggle this switch element</label>
        </div>


        <button  className={"btn btn-info"} type={"submit"}>Save</button>
      </form>
    </div>
  );
}
