import firebaseApp from './firebaseApp'

export default async (collection,data)=>{
return  await firebaseApp.firestore().collection(collection).add({
        ...data,
        created: new Date()
    }); 
}