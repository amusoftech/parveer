import firebaseApp from './firebaseApp'

export default (collection,docId)=>{
    if(docId){
        return firebaseApp.firestore().collection(collection).doc(docId);        
    }
return firebaseApp.firestore().collection(collection);
}