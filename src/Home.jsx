import React,{useEffect,useState} from 'react'
import Layout from './Layout'
import List from './component/List'
import firebaseApp from './utils/firebaseApp'
 import Form from './component/Form'
import readData from './utils/readData'
import writeData from './utils/writeData'
import { connect, useDispatch } from 'react-redux'
import { setUsers } from './redux/actions'

 function Home(props) {
const dispatch=  useDispatch()
  
 const [listOfFierstoreUsers, setListOfFierstoreUsers] = useState([])

 const loadUserdata = ()=>{

  const testCollection =   firebaseApp.firestore().collection('test');  
  testCollection.get().then(docs=>{ 
    let tmpUser=[]
    docs.forEach(doc=>{
       
      tmpUser.push({userId:doc.id,  ...doc.data()})
    })
    setListOfFierstoreUsers(tmpUser)
    dispatch(setUsers(tmpUser))
  }) 
 }
  useEffect(() => { 
    loadUserdata()
  }, [  ])

  console.log("props",props);
  const onSubmit = async (evt) =>{
    evt.preventDefault()  
     const res =
     await firebaseApp.firestore().collection('test').add({
      name:evt.target.name.value,
      country: evt.target.city.value,
      img: 'https://i.pravatar.cc/300'
    });     /* writeData('test',{
      name:evt.target.name.value,
      country: evt.target.city.value,
      img: 'https://i.pravatar.cc/300' 
    })  */ 
    loadUserdata() 
  }
 
  const updateDocument =async (docId) =>{
    console.log("docId",docId);
    const res = await firebaseApp.firestore().collection('test').doc(docId).set({
      img:'https://i.pravatar.cc/300' 
    },{
      merge:true
    })
    loadUserdata() 
  }
    return (
        <Layout {...props}  >
             <List update={updateDocument}  users={listOfFierstoreUsers}/>    

             
             {/* <Form onSubmit={onSubmit}/>    */}
        </Layout>
        
    )
} 

const mapStateToProps = state => ({
  ... state 
}); 
export default connect( mapStateToProps,null)(Home)