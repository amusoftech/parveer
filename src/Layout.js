import React, { Component ,Fragment} from "react";
import ReactDOM from "react-dom";
import "./Layout.css";
import Navbar from "./Navbar";
 

class Layout extends Component {
  render() {

    const { clickMe, myValue, children ,history} = this.props;
     
    return (
      <Fragment>
        <div className="container">
          <Navbar history={history}></Navbar>

          <div class="main">
            <h1>React JS </h1>
            {children}
          </div>
        </div>

        <div class="footer-copyright text-center py-3">
          © 2020 Copyright:
          <a href="https://mdbootstrap.com/">ReactJs.com</a>
        </div>
      </Fragment>
    );
  }
}

export default Layout;
