import React from 'react'
import Layout from './Layout'

export default function Albums(props) {
    //console.log("props",props.match.params.albumId);
    return (

        <Layout {...props} >
            <div>
                <h1>From Album id is : {props.match.params.albumId}</h1>
            </div>
        </Layout>
    )
}
