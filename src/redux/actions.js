const { SET_USERS } = require("./actionTypes");

export const setUsers = (users) =>({ 
    type:SET_USERS,
    payload:users
})