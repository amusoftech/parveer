import React from "react";
import "./App.css";
import Layout from "./Layout";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./Home";
import Albums from "./Albums";
import PageOne from "./pages/PageOne";
import PageTwo from "./pages/PageTwo";
import Update from "./component/Update";
import AnotherPage from "./component/AnotherPage";
import { Provider } from "react-redux";
import store from "./redux/store";

const myRoutes = [
  {
    path: "/page-1",
    component: PageOne,
    layout: Layout,
  },
  {
    path: "/page-2",
    component: PageTwo,
    layout: Layout,
  },
  {
    path: "/update/:user?",
    component: Update,
    layout: Layout,
  },
  {
    path: "/another",
    component: AnotherPage,
    layout: Layout,
  },
];
class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      myValue: true,
      myValue1: false,
      myValue3: false,
      myValue4: false,
    };
  }

  clickMe = () => {
    const { myValue } = this.state;
    this.setState({
      myValue: !myValue,
    });
  };

  render() {
    const { myValue, myValue1 } = this.state;
    return (
      <Provider store={store}>  
        {/* <PageOne/> */}
        <Router>
          <Switch>
            <Route exact path={"/"} component={Home} />
            {myRoutes.map((rote_, key) => {
              return (
                <Route
                  path={rote_.path}
                  render={(props) => (
                    <rote_.layout {...props}>
                      <rote_.component {...props} />
                    </rote_.layout>
                  )}
                />
              );
            })}
          </Switch>
        </Router>
      </Provider>
    );
  }
}

export default App;
